const request = require('supertest');
const app = require('../app/app');
const chai = require('chai');
const should = chai.should();
const config = require('../app/config/config');
const expect = chai.expect;
const Sequelize = require('Sequelize');
const models = require('../app/models/index');

const jwt = require('jsonwebtoken');
let authToken = "Bearer " + jwt.sign({}, config.token.secret);

exports.sequelize = describe('Sequelize Tests', () => {
    describe('Initialize', () => {
        it('It should initialize', (done) => { 
           models.initialize();
           done();
       });
    });
    describe('User', () => {
       it('It should be defined', (done) => { 
           done();
       });
    });
});
const request = require('supertest');
const app = require('../app/app');
const chai = require('chai');
const should = chai.should();
const config = require('../app/config/config');
const expect = chai.expect;
const jwt = require('jsonwebtoken');

let authToken = "Bearer " + jwt.sign({}, config.token.secret);

exports.apiRoutes = describe('Api Routes', () => {
  describe('/users', () => {
    describe('/GET gets all user resources', () => {
      it('it should GET all user objects', (done) => {
        request(app)
          .get('/'+config.apiVersion+'/users')
          .set('authorization',authToken)
          .end((err, res) => {
            res.status.should.equal(200);
            res.body.should.be.a('array');
            done(err);
          });
      });
    });
    describe('/POST generate new user record', () => {
      it('it should GET user object', (done) => {
        request(app)
          .post('/'+config.apiVersion+'/users')
          .set('Authorization',authToken)
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .send({
            'firstName':'Test',
            'lastName': 'User',
            'password': 'test',
            'emailAddress': 'test.user'+(Math.floor(Math.random() * (2000 - 1)) + 1)+'@gmail.com'
          })
          .end((err, res) => {
            res.body.should.have.property('userId');
            res.body.should.have.property('userUUID');
            res.body.should.have.property('firstName');
            res.body.should.have.property('lastName');
            res.body.should.have.property('emailAddress');
            res.body.should.have.property('updatedAt');
            res.body.should.have.property('createdAt');
            done(err);
          });
      });
    });
  });
});

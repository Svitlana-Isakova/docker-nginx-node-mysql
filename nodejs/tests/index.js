if(process.env.NODE_ENV){
    process.env.NODE_ENV = 'testing';
}

require('./api.test');
require('./authentication');
require('./sequelize');
const models = require('../../models/index').models;

module.exports.getUsers = (req, res, next) => {
    models.Group.find({where: {groupId: parseInt(req.params.groupId)}}).then(group=>{
        group.getUsers().then(users=>{
            let data = {};
            res.status(200);
            res.json(users);
        });
    });
}
const express = require('express');
const authMiddleware = require('../authentication');
const authenticate = require('./authenticate');
const developer = require('./developer');

// ================== Authenticate ===================
let AuthenticateRoutes = express.Router();
AuthenticateRoutes.post('/', authMiddleware.requireApiKey, authenticate.post);
AuthenticateRoutes.get('/', authMiddleware.requireToken, authenticate.status);

// ================== Developer ===================
let DeveloperRoutes = express.Router();
DeveloperRoutes.get('/db/sync', authMiddleware.requireToken, developer.syncDB);
DeveloperRoutes.get('/db/drop', authMiddleware.requireToken, developer.dropDB);
DeveloperRoutes.get('/db/seed', authMiddleware.requireToken, developer.seedDB);

// ================== Groups ===================
let GroupRoutes = express.Router();
const groups = require('./v1.0/groups');
GroupRoutes.get('/:groupId/users', groups.getUsers);

let ApiRouter = express.Router();
ApiRouter.use('/authenticate', AuthenticateRoutes);
ApiRouter.use('/developers', DeveloperRoutes);
ApiRouter.use('/groups', GroupRoutes);

module.exports = ApiRouter;

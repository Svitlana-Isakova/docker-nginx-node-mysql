const jwt = require('jsonwebtoken');
const config = require('../config/config');
const Users = require('../models/index').User;

module.exports.post = function(req, res, next) {
  let errors = [];
  if(!req.body.emailAddress){
    errors.push({message: "emailAddress parameter is not set"});
  }
  if(!req.body.password){
    errors.push({message: "password parameter is not set"});
  }
  if(errors.length > 0){
    res.status(400);
    res.json({
      errors: errors
    });
  }
  else{
    Users.findOne({
      where: {emailAddress: req.body.emailAddress},
      attributes: ['userId', 'firstName', 'lastName', 'emailAddress', 'createdAt', 'updatedAt']
    }).then(user => {
      if(user){
        user.dataValues.token = jwt.sign({ userId: user.id, emailAddress: user.emailAddress, createdAt: user.createdAt }, config.token.secret);
        res.status(200);
        res.json({
          data: user
        });
      }
      else{
        res.status(400);
        res.json({
          errors: [
            {status: "400", message: "Account with that email address does not exist"}
          ]
        });
      }
    });
  }
};

module.exports.status = function(req, res, next) {
  res.status(200);
  res.json({
    data: {message: req.token}
  });
};
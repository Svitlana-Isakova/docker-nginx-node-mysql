const app = require('../../app');
const epilogue = require('epilogue');
const models = require('../../models/index').models;
const config = require('../../config/config');

let groups = epilogue.resource({
  model: models.Group,
  endpoints: [
    '/'+config.apiVersion+'/groups',
    '/'+config.apiVersion+'/groups/:groupId'
  ],
  associations: true,
  search: [{
    operator: '$eq',
    param: 'groupId',
    attributes: ['groupId']
  }]
});

groups.list.fetch.before((req, res, context) => {
  //Logger.debug(context.instance);
  return context.continue;
});

module.exports = groups;

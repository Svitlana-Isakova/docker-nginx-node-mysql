const express = require('express');
const router = require('./routes/router');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const app = express();
const config = require('./config/config');
const Logger = require('./libs/Logger');
const epilogueResources = require('./routes/epilogue');
const port = process.env.PORT || 3000;

if(!process.env.NODE_ENV){
  process.env.NODE_ENV = config.defaultEnv || 'production';
}

global.Logger = Logger;

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if(process.env.NODE_ENV !== 'testing'){
  app.use(morgan('combined'));
}

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
});

require("./models/index").initialize();

app.get('/', (req, res) => {
  res.status(200);
  res.json({
    apiVersion: config.apiVersion,
    data: {
      status: '200',
      message: 'Welome to the API'
    }
  });
});

app.use('/'+config.apiVersion, router);
// Initialize epilogue routes
epilogueResources.initialize(app);

// Base catch error handling
app.use((err, req, res, next) => {
  Logger.debug(err.stack);
  delete err.stack; // TODO: Log this when in a dev environment
  res.status(err.statusCode || 500).json({
    apiVersion: config.apiVersion,
    errors: [{
      status: err.statusCode,
      message: err.message,
    }]
  });
});

app.listen(port, () => {
  Logger.info('Api started on port ' + port + ' in ' + process.env.NODE_ENV + ' mode');
});

app.on('error', () =>{
  if (error.syscall !== 'listen') {
    throw error;
  }
  let bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
  switch (error.code) {
  case 'EACCES':
    Logger.error(bind + ' requires elevated privileges');
    process.exit(1);
    break;
  case 'EADDRINUSE':
    Logger.error(bind + ' is already in use');
    process.exit(1);
    break;
  default:
    throw error;
  }
});

// app.on('listening', () => {
//   let addr = server.address();
//   let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
//   Logger.info('Listening on ' + bind);
// });

process.on('uncaughtException', error => {
  Logger.debug(error);
  Logger.error(error.stack);
  Logger.info('Uncaught exception... continuing');
});

module.exports = app;

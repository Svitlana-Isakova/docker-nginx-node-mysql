module.exports = function Unauthorized(message) {
  Error.captureStackTrace(this, this.constructor);

  this.name = this.constructor.name;
  this.message = message || 'Unauthorized';
  this.statusCode = 401;
};

require('util').inherits(module.exports, Error);

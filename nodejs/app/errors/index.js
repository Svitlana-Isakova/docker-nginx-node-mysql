const ErrorPaths = {
  NotFound: 'not-found',
  InternalServerError: 'internal-server-error',
  Forbidden: 'forbidden',
  Unauthorized: 'unauthorized'
};

const EMPTY_RESPONSE = 'EmptyResponse';

Object.keys(ErrorPaths).forEach(key => {
  module.exports[key] = require('./' + ErrorPaths[key]);
});

// For generic responses, if you aren't getting fancy
module.exports.handle = (err, next) => {
  Logger.error(err);
  if (err.message === EMPTY_RESPONSE) {
    return next(new module.exports.NotFound());
  }

  return next(new module.exports.InternalServerError());
};

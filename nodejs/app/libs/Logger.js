var winston = require('winston');
const moment = require('moment');
winston.emitErrs = true;

var logger = null;
switch(process.env.NODE_ENV){
  case 'testing':
    logger = new winston.Logger({
      transports: [
        new winston.transports.Console({
          timestamp: () => {
            return moment().format('YYYY-MM-DD h:mm:ss Z');
          },
          level: 'debug',
          handleExceptions: true,
          humanReadableUnhandledException: true,
          json: false,
          colorize: true
        })
      ],
      exitOnError: false
    });
    break;
  case 'development':
    logger = new winston.Logger({
      transports: [
        new winston.transports.Console({
          timestamp: () => {
            return moment().format('YYYY-MM-DD h:mm:ss Z');
          },
          level: 'debug',
          handleExceptions: true,
          humanReadableUnhandledException: true,
          json: false,
          colorize: true
        })
      ],
      exitOnError: false
    });
    break;
  case 'production':
    logger = new winston.Logger({
      transports: [
        new winston.transports.Console({
          timestamp: () => {
            return moment().format('YYYY-MM-DD h:mm:ss Z');
          },
          level: 'info',
          handleExceptions: true,
          humanReadableUnhandledException: true,
          json: false,
          colorize: true
        })
      ],
      exitOnError: false
    });
    break;
  default:
    logger = new winston.Logger({
      transports: [
        new winston.transports.Console({
          timestamp: () => {
            return moment().format('YYYY-MM-DD h:mm:ss Z');
          },
          level: 'debug',
          handleExceptions: true,
          humanReadableUnhandledException: true,
          json: false,
          colorize: true
        })
      ],
      exitOnError: false
    });
}

module.exports = logger;

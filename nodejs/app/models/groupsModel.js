const Sequelize = require('sequelize'),
  sequelize = require('../config/dbCrud.js'),
  Logger = require('../libs/Logger');

let groupsModel = sequelize.define('groups', {
  groupId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  description: {
    type: Sequelize.TEXT,
    allowNull: true
  },
  slug: {
    type: Sequelize.STRING,
    allowNull: false
  },
  groupUUID: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false
  }
},
{
  timestamps: true,
  freezeTableName: true
});

groupsModel
  .sync() // { force: false }
  .then(function() {
    Logger.debug('Successfully synced groupsModel');
  }).catch(function(err) {
    // handle error
    Logger.error('Error while listening to database', err);
  });

module.exports = groupsModel;

const Sequelize = require('sequelize');
const sequelize = require('../config/dbCrud.js');
const Logger = require('../libs/Logger');

let UserGroup = sequelize.define('UserGroup', {
  userId: {
    type: Sequelize.INTEGER,
    references: {
      model: 'user',
      key: 'userId'
    }
  },
  groupId: {
    type: Sequelize.INTEGER,
    references: {
      model: 'group',
      key: 'groupId'
    }
  }
},
{
  timestamps: true,
  freezeTableName: true,
  tableName: 'user_group'
});

module.exports = UserGroup;

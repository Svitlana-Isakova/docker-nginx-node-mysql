const Sequelize = require('sequelize');
const sequelize = require('../config/dbCrud.js');
const Logger = require('../libs/Logger');


let GroupPermission = sequelize.define('GroupPermission', {
  userId: {
    type: Sequelize.INTEGER,
    references: {
      model: 'group',
      key: 'groupId'
    }
  },
  groupId: {
    type: Sequelize.INTEGER,
    references: {
      model: 'permission',
      key: 'permissionId'
    }
  }
},
{
  timestamps: true,
  freezeTableName: true,
  tableName: 'group_permission'
});

module.exports = GroupPermission;

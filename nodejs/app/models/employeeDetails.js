const Sequelize = require('sequelize');
const sequelize = require('../config/dbCrud.js');
const Logger = require('../libs/Logger');

var EmployeeModel = sequelize.define('employee', {
  employeeId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  employeeName: {
    type: Sequelize.STRING,
    allowNull: false
  }
}, {
  timestamps: true,
  freezeTableName: true
});

EmployeeModel
  .sync() // { force: false }
  .then(function() {
    Logger.debug('Successfully synced EmployeeModel');
  }).catch(function(err) {
    // handle error
    Logger.error('Error while listening to database', err);
  });

module.exports = EmployeeModel;

const Sequelize = require('sequelize');
const sequelize = require('../config/dbCrud.js');
const Logger = require('../libs/Logger');


let Group = sequelize.define('Group', {
  groupId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  description: {
    type: Sequelize.TEXT,
    allowNull: true
  },
  slug: {
    type: Sequelize.STRING,
    allowNull: false
  },
  groupUUID: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false
  }
},
{
  timestamps: true,
  freezeTableName: true,
  tableName: 'group',
  name: {
    singular: 'group',
    plural: 'groups',
  }
});

module.exports = Group;

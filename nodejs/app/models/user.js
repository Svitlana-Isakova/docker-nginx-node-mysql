const Sequelize = require('sequelize');
const sequelize = require('../config/dbCrud.js');
const Logger = require('../libs/Logger');


let User = sequelize.define('User', {
  userId: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  firstName: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notEmpty: true
    }
  },
  lastName: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      notEmpty: true
    }
  },
  emailAddress: {
    type: Sequelize.STRING,
    allowNull: false,
    validate:{
      isEmail : true,
      notEmpty: true
    }
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
    validate:{
      notEmpty: true,
      min: 6
    }
  },
  userUUID: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false
  }
},
{
  timestamps: true,
  freezeTableName: true,
  tableName: 'user',
  name: {
    singular: 'user',
    plural: 'users',
  }
});

module.exports = User;

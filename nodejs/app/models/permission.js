const Sequelize = require('sequelize'),
  sequelize = require('../config/dbCrud.js'),
  Logger = require('../libs/Logger');

  let Permission = sequelize.define('Permission', {
    permissionId: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    description: {
      type: Sequelize.STRING,
      allowNull: false
    }
  },
  {
    timestamps: true,
    freezeTableName: true,
    name: {
      singular: 'perission',
      plural: 'permissions',
    }
});

module.exports = Permission;